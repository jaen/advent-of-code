#!/usr/bin/env boot

(set-env! :dependencies '[[funcool/cuerdas "0.7.0"]])
(require '[cuerdas.core :as str])

(def input (str/replace (slurp "input") "\n" ""))

(defn next-coords [coords direction]
  (mapv + coords
    (condp = direction
      \^ [ 1  0]
      \v [-1  0]
      \< [ 0 -1]
      \> [ 0  1])))

(defn presents-per-house [input]
  (loop [coords [0 0]
         houses {[0 0] 1}
         [direction & rest] input]
    (let [next-coords (next-coords coords direction)
          next-houses (update houses next-coords (comp inc #(or % 0)))]
          (if (seq rest)
            (recur next-coords next-houses rest)
            next-houses))))

(defn swap-active [active]
  (condp = active
    :santa :robot
    :robot :santa))

(defn presents-per-house-with-robot [input]
  (loop [coords {:santa [0 0] :robot [0 0]}
         active :santa
         houses {[0 0] 2}
         [direction & rest] input]
    (let [next-coords (next-coords (active coords) direction)
          next-houses (update houses next-coords (comp inc #(or % 0)))]
          (if (seq rest)
            (recur (assoc coords active next-coords)
                   (swap-active active)
                   next-houses
                   rest)
            next-houses))))

(defn -main [& args]
  (println "TOTAL PRESENTS: "         (count (presents-per-house input)))
  (println "TOTAL PRESENTS W/ROBOT: " (count (presents-per-house-with-robot input))))
