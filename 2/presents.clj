#!/usr/bin/env boot

(set-env! :dependencies '[[funcool/cuerdas "0.7.0"]])
(require '[cuerdas.core :as str])

(defn integer [val]
  (Integer. val))

(defn read-input []
  (let [input (slurp "input")]
    (for [line (str/lines input)]
      (mapv integer (str/split (str/trim line) "x")))))

(def input (read-input))

(defn rotate [n s]
  (lazy-cat (drop n s)
            (take n s)))

(defn side-areas [sides]
  (map #(* %1 %2) sides (rotate 1 sides)))

(defn volume [sides]
  (reduce * sides))

(defn needed-paper [sides]
  (let [side-areas (side-areas sides)
        least-side (apply min side-areas)]
    (+ (reduce + (map #(* 2 %) side-areas))
       least-side)))

(defn needed-ribbon [sides]
  (let [shortest-perimeter (take 2 (sort sides))]
    (+ (reduce + (map #(* % 2) shortest-perimeter))
       (volume sides))))

(defn total-paper-needed [boxes]
  (reduce + (map needed-paper boxes)))

(defn total-ribbon-needed [boxes]
  (reduce + (map needed-ribbon boxes)))

(defn -main [& args]
  (println "TOTAL PAPER: " (total-paper-needed input))
  (println "TOTAL RIBBON: " (total-ribbon-needed input)))
