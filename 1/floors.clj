#!/usr/bin/env boot

(set-env! :dependencies '[[funcool/cuerdas "0.7.0"]])
(require '[cuerdas.core :as str])

(def input (str/replace (slurp "input") "\n" ""))

(defn new-floor [direction floor]
  (condp = direction
    \( (inc floor)
    \) (dec floor)))

(defn floor-seq [directions]
  (loop [[direction & rest] directions
         [last-floor & _ :as floors] '(0)]
    (if direction
      (recur rest (cons (new-floor direction last-floor) floors))
      (reverse floors))))

(defn final-floor [input]
  (last (floor-seq input)))

; (def (map-indexed vector floor-seq))
(defn when-in-basement? [input]
  (some (fn [[id floor]]
          (when
            (neg? floor) id))
    (map-indexed vector (floor-seq input))))

(defn -main [& args]
  (println "FINAL FLOOR: " (final-floor input))
  (println "WHEN IN BASEMENT: " (when-in-basement? input)))
