#!/usr/bin/env boot

(set-env! :dependencies '[[funcool/cuerdas "0.7.0"]
                          [net.mikera/core.matrix "0.47.0"]
                          [net.mikera/vectorz-clj "0.38.0"]
                          [clatrix "0.5.0"]])

(require '[cuerdas.core :as str]
         '[clojure.core.matrix :as m]
         '[clojure.core.matrix.select :as ms]
         '[clojure.core.matrix.operators :as mo]
         '[clatrix.core])

(m/set-current-implementation :vectorz)

(defn integer [val]
  (Integer. val))

(def line-regex
  #"(turn on|turn off|toggle)\s+(\d+),(\d+)\s+through\s+(\d+),(\d+)")

(defn parse-line [line]
  (let [[_ action x-start y-start x-end y-end] (re-matches line-regex line)]
    [(keyword (str/replace action "turn " ""))
     [(integer x-start) (integer y-start)]
     [(integer x-end)   (integer y-end)]]))

(defn read-input []
  (let [input (slurp "input")]
    (for [line (str/lines input)]
      (parse-line (str/trim line)))))

(def input (read-input))

(def ON   1.0)
(def OFF -1.0)

(defn new-grid [n fill]
  (m/matrix (take n (repeat (take n (repeat fill))))))

(defn inversion-matrix [rows colls coords-x coords-y]
  (m/set-selection!
    (m/assign! (m/new-matrix rows colls) 1)
      coords-x coords-y -1))

(defn resulting-grid [instructions]
  (let [N 1000
        grid (new-grid N OFF)]
    (doseq [[action [start-x start-y] [stop-x stop-y]] instructions
            :let [coords-x (range start-x (inc stop-x))
                  coords-y (range start-y (inc stop-y))]]
      ; (println "doing " action "at" [start-x start-y] [stop-x stop-y])
      (condp = action
        :on     (m/set-selection! grid coords-x coords-y 1)
        :off    (m/set-selection! grid coords-x coords-y -1)
        :toggle (m/mul! grid (inversion-matrix N N coords-x coords-y))))
    grid))

(defn count-on [instructions]
  (count (filter #(> % 0)
         (seq (m/to-vector (resulting-grid instructions))))))

(defn clamp-to-zero! [matrix]
  (mo/max matrix 0))

(defn brightness-matrix [rows colls coords-x coords-y value]
  (m/set-selection!
    (m/assign! (m/new-matrix rows colls) 0)
      coords-x coords-y value))

(defn resulting-grid' [instructions]
  (let [N 1000
        grid (new-grid N 0)]
    (doseq [[action [start-x start-y] [stop-x stop-y]] instructions
            :let [coords-x (range start-x (inc stop-x))
                  coords-y (range start-y (inc stop-y))]]
      ; (println "doing " action "at" [start-x start-y] [stop-x stop-y])
      (condp = action
        :on     (m/add! grid (brightness-matrix N N coords-x coords-y 1))
        :off    (m/assign! grid (clamp-to-zero! (m/sub! grid (brightness-matrix N N coords-x coords-y 1))))
        :toggle (m/add! grid (brightness-matrix N N coords-x coords-y 2))))
    grid))

(defn total-brightness [instructions]
  (Math/ceil (m/esum (resulting-grid' instructions))))

(defn -main [& args]
  (time (println "LIGHTS ON: " (count-on input)))
  (time (println "TOTAL BRIGHTNESS: " (total-brightness input))))
