#!/usr/bin/env boot

(set-env! :dependencies '[[funcool/cuerdas "0.7.0"]
                          [buddy/buddy-core "0.8.1"]])

(require '[cuerdas.core :as str]
         '[buddy.core.hash :as hash]
         '[buddy.core.codecs :as hash-codec])

(def secret-key (str/replace (slurp "input") "\n" ""))

(defn matches? [str lead]
  (str/starts-with? str lead))

(defn hash-of [str]
  (hash-codec/bytes->hex (hash/md5 str)))

(defn find-number [lead]
  (some (fn [number]
          (let [string (str secret-key number)]
            (when (matches? (hash-of string) lead)
              number)))
        (range)))

(defn -main [& args]
  (println "W/5 ZEROS: " (find-number "00000"))
  (println "W/6 ZEROS: " (find-number "000000")))
