#!/usr/bin/env boot

(set-env! :dependencies '[[funcool/cuerdas "0.7.0"]])

(require '[cuerdas.core :as str])

(def input (str/lines (slurp "input")))

(def naughty-pairs #{[\a \b]
                     [\c \d]
                     [\p \q]
                     [\x \y]})

(defn nice? [word]
  (let [vowel-count (count (filter #{\a\i\e\o\u} word))
        double-letter? (boolean
                         (some (fn [[a b]] (= a b))
                             (partition 2 1 word)))
        not-pairs? (not (some naughty-pairs (partition 2 1 word)))]
    (and (>= vowel-count 3)
         double-letter?
         not-pairs?)))

(defn count-nice [words]
  (count (filter nice? words)))

(defn alt-nice? [word]
  (let [repeats? (boolean (re-find #"(\w\w).*\1" word))
        triple?  (boolean (re-find #"(\w)\w\1" word))]
    (and repeats?
         triple?)))

(defn count-alt-nice [words]
  (count (filter alt-nice? words)))

(defn -main [& args]
  (println "NICE: " (count-nice input))
  (println "ALT NICE: " (count-alt-nice input)))
